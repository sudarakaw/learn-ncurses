#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  // NOTE: all measurements are in ROWS/COLUMNS
  int height = 10, width = 20, start_y = 10, start_x = 10;

  // Define window
  WINDOW *win = newwin(height, width, start_y, start_x);

  // Refresh whole screen (render buffered changes on screen)
  refresh();

  // Draw box around given window
  box(win, 0, 0);

  //Print text in window
  /* wprintw(win, "On the border"); */
  mvwprintw(win, 1, 1, "Inside the Box");

  // Refresh given window (render buffered changes in window)
  wrefresh(win);

  getch();

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
