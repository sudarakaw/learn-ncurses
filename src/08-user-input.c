#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  cbreak();
  noecho();

  int yMax, xMax;
  getmaxyx(stdscr, yMax, xMax);

  WINDOW *win = newwin(3, xMax - 12, yMax - 5, 5);
  box(win, 0, 0);

  refresh();
  wrefresh(win);

  // Allow us to use arrow keys
  keypad(win, true);

  int c = wgetch(win);

  if(KEY_UP == c) {
    mvwprintw(win, 1, 1, "You pressed up!");

    wrefresh(win);
  }

  getch();

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
