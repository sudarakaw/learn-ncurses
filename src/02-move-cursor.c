#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  // move cursor to (Y, X) or (ROW, COLUMN) position.
  // NOTE: (Y, X) and not (x, y)
  int x, y;

  x = y = 10;

  move(y, x);

  // Print a string (const char *) to current window (screen)
  printw("NCURSES Yo!");

  getch();

  // Clear screen
  clear();

  // You can also move & print using `mvprintw` in one go.
  mvprintw(0, 0, "[START");
  mvprintw(23, 80 - 4, "END]");

  // Update the screen to reflect changes in memory buffer.
  refresh();

  int ch = getch();

  // Cleanup NCURSES environment.
  endwin();

  printf("Existing NCURSES by pressing %d\n", ch);

  return 0;
}
