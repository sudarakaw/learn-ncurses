#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  // Treat all inputs as raw input (including Ctrl-C)
  // (Opposite of `cbreak`, and cancels out previously set `cbreak` behaviour.)
  raw();

  // Make NCURSES program exit on Ctrl-C/Break
  // NOTE: This is the default behaviour. Can be used to cancel out previously
  // set `raw` behaviour.
  cbreak();

  // Do NOT automatically print user input to the screen.
  noecho();

  // NOTE: all measurements are in ROWS/COLUMNS
  int height = 10, width = 20, start_y = 10, start_x = 10;

  // Define window
  WINDOW *win = newwin(height, width, start_y, start_x);

  // Refresh whole screen (render buffered changes on screen)
  refresh();

  // Draw box around given window
  box(win, 0, 0);

  //Print text in window
  /* wprintw(win, "On the border"); */
  mvwprintw(win, 1, 1, "Inside the Box");

  // Refresh given window (render buffered changes in window)
  wrefresh(win);

  getch();
  getch();

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
