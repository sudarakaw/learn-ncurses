#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  // NOTE: all measurements are in ROWS/COLUMNS
  int height = 10, width = 20, start_y = 10, start_x = 10;

  // Define window
  WINDOW *win = newwin(height, width, start_y, start_x);

  // Refresh whole screen (render buffered changes on screen)
  refresh();

  // Draw box around given window
  // EXAMPLE: box(WINDOW, VERTICAL(Y)_BORDER, HORIZONTAL(X)_BORDER)
  // NOTE: Border characters are given as integer ASCII values.
  /* box(win, 104, 0); */
  box(win, (int)'*', (int)'~');

  // `wborder` gives more control over the border character to use.
  // EXAMPLE:
  //  wborder(
  //    WINDOW,
  //    LEFT_BORDER,
  //    RIGHT_BORDER,
  //    TOP_BORDER,
  //    BOTTOM_BORDER,
  //    TOP_LEFT_CORNER,
  //    TOP_RIGHT_CORNER,
  //    BOTTOM_LEFT_CORNER,
  //    BOTTOM_RIGHT_CORNER
  //  )
  int
    /* left = (int)'1', */
    /* right = (int)'2', */
    /* top = (int)'3', */
    /* bottom = (int)'4', */
    /* tl = (int)'5', */
    /* tr = (int)'6', */
    /* bl = (int)'7', */
    /* br = (int)'8'; */
    left = (int)'|',
    right = (int)'|',
    top = (int)'-',
    bottom = (int)'-',
    tl = (int)'+',
    tr = (int)'+',
    bl = (int)'+',
    br = (int)'+';

  wborder(win, left, right, top, bottom, tl,tr, bl, br);

  //Print text in window
  /* wprintw(win, "On the border"); */
  mvwprintw(win, 1, 1, "Inside the Box");

  // Refresh given window (render buffered changes in window)
  wrefresh(win);

  getch();

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
