#include <stdio.h>
#include <ncurses.h>

void show_terminal_info(WINDOW *win) {
  int y, x;

  wmove(win, 1, 1);

  // Get cursor position of the cursor (relative to given window window).
  getyx(win, y, x);
  wprintw(win, "Cursor is at %d, %d\n", y, x);

  getyx(win, y, x);
  wprintw(win, "Cursor is at %d, %d\n", y, x);

  // Get top-left position of the given window.
  getbegyx(win, y, x);
  wprintw(win, "Window starts at %d, %d\n", y, x);

  // Get size (ROWS, COLUMNS) of the given window.
  getmaxyx(win, y, x);
  wprintw(win, "Window size is %d x %d\n", y, x);
}

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  cbreak();
  noecho();

  // NOTE: `stdscr` is a reference to the main NCURSES screen.
  show_terminal_info(stdscr);

  WINDOW *win = newwin(30, 50, 15, 20);
  show_terminal_info(win);

  refresh();
  wrefresh(win);

  getch();

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
