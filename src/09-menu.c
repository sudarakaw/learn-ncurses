#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  cbreak();
  noecho();

  int yMax, xMax;
  getmaxyx(stdscr, yMax, xMax);

  WINDOW *win = newwin(6, xMax - 12, yMax - 8, 5);
  box(win, 0, 0);

  refresh();
  wrefresh(win);

  keypad(win, true);

  char *choises[3] = { "Do Something", "Do Another Thing", "Exit" };
  int choice;
  int hightlight = 0;

  while(1) {
    for(int i = 0; i < 3; i++) {
      if(i == hightlight) {
        wattron(win, A_REVERSE);
      }

      mvwprintw(win, i + 1, 1, choises[i]);

      wattroff(win, A_REVERSE);
    }

    wrefresh(win);

    choice = wgetch(win);

    if(KEY_UP == choice) {
      hightlight -= 1;

      if(0 > hightlight) {
        hightlight = 0;
      }
    }
    else if(KEY_DOWN == choice) {
      hightlight += 1;

      if(2 < hightlight) {
        hightlight = 2;
      }
    }
    else if(10 == choice && 2 == hightlight) {
      break;
    }
  }

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
