TARGETS = 01-basics 02-move-cursor 03-windows 04-window-borders 05-environment-behaviour 06-attributes-colors 07-terminal-information 08-user-input 09-menu
LIBS = -lncurses
SRC_DIR = src
DIST_DIR = dist

all: $(TARGETS)
	@echo -e "\nUse \"make <source>\" to build individual example.\n"

$(DIST_DIR):
	mkdir -p $@

$(TARGETS): %: $(DIST_DIR) $(DIST_DIR)/%

$(DIST_DIR)/%: $(SRC_DIR)/%.c
	$(CC) -o $@ $< $(CFLAGS) $(LIBS)

.PHONY: all clean

clean:
	$(RM) -r $(DIST_DIR)
